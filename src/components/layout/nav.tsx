/* eslint-disable @next/next/no-img-element */
"use client";
import { useEffect, useState, useRef } from "react";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { fetcNavData } from "@/src/libs/requests";
import { Asset } from "@/src/interface/Asset";
import { getTotalForField } from "../shared/Summation";
import { Rocket, Bitcoin, Search } from "lucide-react";
import SearchResults from "../ui/SearchResults";

function formatMarketCap(value: number): string {
  if (value >= 1e12) {
    // trillion
    return `$${(value / 1e12).toFixed(2)}T`;
  } else if (value >= 1e9) {
    // billion
    return `$${(value / 1e9).toFixed(2)}B`;
  } else if (value >= 1e6) {
    // million
    return `$${(value / 1e6).toFixed(2)}M`;
  }
  return `$${value.toFixed(2)}`;
}

export default function Nav() {
  const [assets, setAssets] = useState<Asset[]>([]);
  const [TotalMarketCap, setTotalMarketCap] = useState<number>(0);
  const [isSearchOpen, setIsSearchOpen] = useState(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [searchResults, setSearchResults] = useState<Asset[]>([]);
  const router = useRouter();
  const searchRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    fetchData();

    // Click outside to close search
    const handleClickOutside = (event: MouseEvent) => {
      if (
        searchRef.current &&
        !searchRef.current.contains(event.target as Node)
      ) {
        setIsSearchOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => document.removeEventListener("mousedown", handleClickOutside);
  }, []);

  async function fetchData() {
    try {
      const data = await fetcNavData();
      const totalMarketCap = data.reduce((sum: number, asset: Asset) => {
        const marketCap = parseFloat(asset.marketCapUsd) || 0;
        return sum + marketCap;
      }, 0);
      setTotalMarketCap(totalMarketCap);
      setAssets(data);
    } catch (error) {}
  }

  const handleSearch = (e: React.FormEvent) => {
    e.preventDefault();
    if (searchQuery.trim() && searchResults.length > 0) {
      router.push(`/currencies/${searchResults[0].id}`);
      setSearchQuery("");
      setIsSearchOpen(false);
      setSearchResults([]);
    }
  };

  const handleSearchInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const query = e.target.value;
    setSearchQuery(query);

    if (query.trim()) {
      const filtered = assets
        .filter(
          (asset) =>
            asset.name.toLowerCase().includes(query.toLowerCase()) ||
            asset.symbol.toLowerCase().includes(query.toLowerCase())
        )
        .slice(0, 5); // Limit to 5 results
      setSearchResults(filtered);
    } else {
      setSearchResults([]);
    }
  };

  const handleResultClick = () => {
    setIsSearchOpen(false);
    setSearchQuery("");
    setSearchResults([]);
  };

  return (
    <>
      <div className="navbar base-100 sticky top-0 z-40 justify-center bg-base-200">
        <div className="navbar-center flex flex-col lg:flex-row items-center">
          <h3 className="card-title mb-2 lg:mb-0 lg:mr-5 text-sm lg:text-base">
            Market Cap: {formatMarketCap(TotalMarketCap)}
          </h3>

          {/* Search Component */}
          <div className="relative mx-2" ref={searchRef}>
            {isSearchOpen ? (
              <form onSubmit={handleSearch} className="flex items-center">
                <input
                  type="text"
                  value={searchQuery}
                  onChange={handleSearchInput}
                  placeholder="Search coins..."
                  className="input input-bordered input-sm w-full max-w-xs"
                  autoFocus
                />
              </form>
            ) : (
              <button
                onClick={() => setIsSearchOpen(true)}
                className="btn btn-ghost btn-circle"
              >
                <Search className="h-5 w-5" />
              </button>
            )}

            <SearchResults
              results={searchResults}
              isVisible={isSearchOpen && searchResults.length > 0}
              onResultClick={handleResultClick}
            />
          </div>

          <Link
            href="/rally-to-100k"
            className="btn btn-glass normal-case text-xl hidden lg:flex bg-base-100"
          >
            <Bitcoin size={20} /> to $100k <Rocket size={20} />
          </Link>
        </div>
      </div>
    </>
  );
}
