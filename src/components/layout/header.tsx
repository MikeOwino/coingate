/* eslint-disable @next/next/no-img-element */
"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useEffect, useState } from "react";
import Switcher from "@/src/components/themes/Swithcher";
import { BarChart2, Bitcoin, Landmark, BadgeDollarSign } from "lucide-react";

export default function Header() {
  const pathname = usePathname();
  const [currentPath, setCurrentPath] = useState("");

  useEffect(() => {
    setCurrentPath(pathname);
  }, [pathname]);

  const isActive = (path: string) => {
    if (path === "/" && currentPath === "/") return true;
    if (path !== "/" && currentPath.startsWith(path)) return true;
    return false;
  };

  return (
    <>
      <div className="navbar bg-base-300 sticky top-0 z-50 justify-center hidden lg:flex">
        <div className="navbar-center">
          <Link
            href="/"
            className="btn btn-glass normal-case text-xl mb-2 lg:mb-0 lg:mr-2"
          >
            <img
              src="/logo-arrena_rfimdk_ujyzxh.png"
              alt="Logo"
              height={40}
              width={100}
            />
          </Link>
          <Link
            href="/"
            className={`btn btn-ghost btn-outline mr-2 ${
              isActive("/") ? "btn-active" : ""
            }`}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <span>Coins</span>
              <Bitcoin className="h-5" />
            </div>
          </Link>
          <Link
            href="/exchanges"
            className={`btn btn-ghost btn-outline mr-2 ${
              isActive("/exchanges") ? "btn-active" : ""
            }`}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <span>Exchanges</span>
              <Landmark className="h-5" />
            </div>
          </Link>
          <Link
            href="/markets"
            className={`btn btn-ghost btn-outline mr-2 ${
              isActive("/markets") ? "btn-active" : ""
            }`}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <span>Markets</span>
              <BadgeDollarSign className="h-5" />
            </div>
          </Link>
          <Link
            href="/gainers"
            className={`btn btn-ghost btn-outline mr-2 ${
              isActive("/gainers") ? "btn-active" : ""
            }`}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <span>Gainers</span>
              <BarChart2 className="h-5 text-green-700" />
            </div>
          </Link>
          <Link
            href="/losers"
            className={`btn btn-ghost btn-outline mr-2 ${
              isActive("/losers") ? "btn-active" : ""
            }`}
          >
            <div style={{ display: "flex", alignItems: "center" }}>
              <span>Losers</span>
              <BarChart2 className="h-5 text-red-700" />
            </div>
          </Link>
        </div>
      </div>

      {/* Mobile Bottom Navigation */}
      <div className="btm-nav lg:hidden z-50">
        <button className={isActive("/") ? "active" : ""}>
          <Link href="/">
            <div className="flex flex-col items-center">
              <Bitcoin className="h-5 w-5" />
              <span className="btm-nav-label">Coins</span>
            </div>
          </Link>
        </button>

        <button className={isActive("/exchanges") ? "active" : ""}>
          <Link href="/exchanges">
            <div className="flex flex-col items-center">
              <Landmark className="h-5 w-5" />
              <span className="btm-nav-label">Exchanges</span>
            </div>
          </Link>
        </button>

        <button className={isActive("/markets") ? "active" : ""}>
          <Link href="/markets">
            <div className="flex flex-col items-center">
              <BadgeDollarSign className="h-5 w-5" />
              <span className="btm-nav-label">Markets</span>
            </div>
          </Link>
        </button>

        <button className={isActive("/gainers") ? "active" : ""}>
          <Link href="/gainers">
            <div className="flex flex-col items-center">
              <BarChart2 className="h-5 w-5 text-green-700" />
              <span className="btm-nav-label">Gainers</span>
            </div>
          </Link>
        </button>

        <button className={isActive("/losers") ? "active" : ""}>
          <Link href="/losers">
            <div className="flex flex-col items-center">
              <BarChart2 className="h-5 w-5 text-red-700" />
              <span className="btm-nav-label">Losers</span>
            </div>
          </Link>
        </button>
      </div>
    </>
  );
}
