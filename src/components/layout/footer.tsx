import Link from "next/link";
import Switcher from "@/src/components/themes/Swithcher";
import Image from "next/image";

export default function Footer() {
  return (
    <>
      <footer className="footer p-10 bg-base-200 text-base-content mb-16 lg:mb-0">
        <aside>
          <Link href="/">
            <Image
              src="/logo-arrena_rfimdk_ujyzxh.png"
              alt="logo"
              height={50}
              width={100}
            />
          </Link>
        </aside>
        <nav>
          <p className="font-bold">
            Created with Love for Cypto by{" "}
            <a
              className="text-red-600"
              href="https://mikeowino.com/"
              target="_blank"
            >
              Mikey
            </a>
          </p>
        </nav>
        <nav>
          <p>
            Arena Inc.
            <br />
            Providing reliable tech since 2020
          </p>
        </nav>
        <nav>
          <Switcher />
        </nav>
      </footer>
    </>
  );
}
