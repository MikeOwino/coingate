"use client";

import { useState, useEffect } from "react";
import { themes } from "@/tailwind.config";

export default function Switcher() {
  const [theme, setTheme] = useState("light");

  useEffect(() => {
    // Get the saved theme from localStorage or default to 'light'
    const savedTheme = window.localStorage.getItem("theme") || "light";
    setTheme(savedTheme);
    document.documentElement.setAttribute("data-theme", savedTheme);
  }, []);

  const handleThemeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const newTheme = e.target.value;
    setTheme(newTheme);
    document.documentElement.setAttribute("data-theme", newTheme);
    window.localStorage.setItem("theme", newTheme);
  };

  return (
    <select
      className="select select-bordered select-sm w-32"
      value={theme}
      onChange={handleThemeChange}
    >
      {themes.map((theme) => (
        <option key={theme} value={theme}>
          {theme}
        </option>
      ))}
    </select>
  );
}
