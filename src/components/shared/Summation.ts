/**
 * Calculates the total value for a given field across an array of items.
 *
 * @param items - The array of items to calculate the total for.
 * @param field - The field name to sum the values from.
 * @returns The total value for the specified field across all items.
 */
export function getTotalForField(items: any[], field: string): number {
    let total = 0;

    items.forEach((item) => {
        if (!item[field] || isNaN(item[field])) {
            return;
        }
        total += item[field];
    });
    return total;
}
