"use client";

import { useEffect, useState } from "react";
import Loading from "@/src/components/loaders/loading";
import { useRouter } from "next/navigation";
import { Asset } from "@/src/interface/Asset";
import { fetchTrending } from "@/src/libs/requests";

export default function HotList() {
  const [assets, setAssets] = useState<Asset[]>([]);
  const router = useRouter();

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    // setIsLoading(true);
    try {
      const data = await fetchTrending();
      setAssets(data);
    } catch (error) {}
  }

  if (!assets.length) {
    return <Loading />;
  }

  // Filter assets where changePercent24Hr is more than 5
  const filteredAssets = assets.filter(
    (asset) => parseFloat(asset.changePercent24Hr) > 0
  );
  // Sort the filteredAssets array in ascending order by change percent.
  filteredAssets.sort(
    (a, b) => parseFloat(b.changePercent24Hr) - parseFloat(a.changePercent24Hr)
  );

  if (!assets.length) {
    return <Loading />;
  }
  return (
    <>
      <div className="min-h-screen h-max">
        <h1 className="text-3xl font-bold text-center">
          Trending Coins of The Day
        </h1>
        <section className="m-2 grid overflow-hidden grid-cols-2 sm:grid-cols-3 md:grid-cols-4 lggrid-cols-5 xl:grid-cols-6 2xl:grid-cols-7 gap-2">
          {filteredAssets.map((asset) => (
            <div key={asset.id} className="card h-56 bg-base-300 m-1">
              <div className="card-body">
                <h2
                  className="card-title cursor-pointer"
                  onClick={() => router.push(`/currencies/${asset.id}`)}
                >
                  <span className="text-green-500 underline hover:text-blue-500">
                    {asset.name}
                  </span>
                </h2>
                {/* <p className="font-medium">Symbol: </p> */}
                <p className="font-medium">
                  Price:
                  <span className="font-extrabold ml-1">
                    $
                    {!isNaN(parseFloat(asset.priceUsd))
                      ? parseFloat(asset.priceUsd).toLocaleString(undefined, {
                          minimumFractionDigits: 4,
                          maximumFractionDigits: 4,
                        })
                      : "N/A"}
                  </span>
                </p>
                <p className="font-medium">
                  24Hrs:
                  <span className="font-extrabold ml-1">
                    {!isNaN(parseFloat(asset.changePercent24Hr))
                      ? `${parseFloat(asset.changePercent24Hr).toFixed(2)}`
                      : "N/A"}
                  </span>
                  %
                </p>
                {/* Add more asset information here */}
                <div className="card-actions justify-end">
                  {/* <a
                    href={`https://www.binance.com/en/trade/${asset.symbol.toUpperCase()}_USDT?theme=dark&type=spot`}
                    target="_blank"
                  >
                    <button className="btn btn-sm btn-neutral">
                      Buy {asset.symbol}
                    </button>
                  </a> */}
                  {/* <a
                    href={`https://coinmarketcap.com/currencies/${asset.name
                      .toLowerCase()
                      .replace(/\s+/g, "-")}`}
                    target="_blank"
                  > */}
                  <a
                    href={`https://www.bybit.com/trade/usdt/${asset.symbol}USDT`}
                    target="_blank"
                  >
                    <button className="btn btn-sm btn-neutral">
                      Trade {asset.symbol}
                    </button>
                  </a>
                </div>
              </div>
            </div>
          ))}
        </section>
      </div>
    </>
  );
}
