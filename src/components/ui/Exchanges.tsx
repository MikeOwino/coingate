/* eslint-disable @next/next/no-img-element */
"use client";
import { useEffect, useState } from "react";
import Link from "next/link";
import { SquareArrowOutUpRight } from "lucide-react";
import Loading from "@/src/components/loaders/loading";
import { Exchange } from "@/src/interface/Exchange";
import { fetchExchange } from "@/src/libs/requests";

export default function ExchangeData() {
  const [exchanges, setExchanges] = useState<Exchange[]>([]);

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    // setIsLoading(true);
    try {
      const data = await fetchExchange();
      setExchanges(data);
    } catch (error) {}
  }

  if (!exchanges.length) {
    return <Loading />;
  }

  return (
    <>
      <div className="overflow-x-auto bg-base-200 w-full">
        <div className="max-w-[100vw]">
          <table className="table table-pin-rows">
            {/* head */}
            <thead>
              <tr className="font-bold">
                <th className="!static">
                  <label>rank</label>
                </th>
                <th className="!static">Exchange</th>
                <th className="!static">Volume</th>
                <th className="hidden md:table-cell">Vol %</th>
                <th className="hidden md:table-cell">Pairs</th>
                <th className="!static">Link</th>
              </tr>
            </thead>
            <tbody>
              {exchanges.slice(0, 10).map((exchange) => (
                <tr key={exchange.name}>
                  <th className="!static">
                    <label>{exchange.rank}</label>
                  </th>
                  <td className="!static">
                    <Link href={`/exchanges/${exchange.exchangeId}`}>
                      <div className="flex items-center space-x-3">
                        <div className="avatar">
                          <div
                            className={`${
                              parseFloat(exchange.percentTotalVolume) > 0
                                ? "ring-success"
                                : "ring-error"
                            } rounded-full ring ring-offset-base-100 ring-offset-1 w-8 md:w-12 h-8 md:h-12`}
                          >
                            <img
                              src="https://ik.imagekit.io/20xgx5cbk/crypto/btc@2x.png"
                              alt={exchange.exchangeId}
                              height={500}
                              width={500}
                              className="w-full h-full bg-zinc-300 rounded-lg dark:bg-zinc-600"
                            />
                          </div>
                        </div>
                        <div>
                          <div className="font-bold text-sm md:text-base">{exchange.name}</div>
                          <div className="text-xs md:text-sm opacity-50">
                            {exchange.exchangeId}
                          </div>
                        </div>
                      </div>
                    </Link>
                  </td>
                  <td className="font-bold text-sm md:text-base !static">
                    ${!isNaN(parseFloat(exchange.volumeUsd))
                      ? parseFloat(exchange.volumeUsd).toLocaleString(undefined, {
                          minimumFractionDigits: 0,
                          maximumFractionDigits: 0,
                        })
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-bold">
                    {!isNaN(parseFloat(exchange.percentTotalVolume))
                      ? parseFloat(exchange.percentTotalVolume).toFixed(2)
                      : "N/A"}
                    %
                  </td>
                  <td className="hidden md:table-cell font-medium">
                    {exchange.tradingPairs}
                  </td>
                  <td className="!static">
                    <a 
                      href={exchange.exchangeUrl} 
                      target="_blank"
                      className="btn btn-ghost btn-sm"
                    >
                      <SquareArrowOutUpRight className="h-4 w-4 md:h-5 md:w-5" />
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
