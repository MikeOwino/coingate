/* eslint-disable @next/next/no-img-element */
"use client";
import { useRouter } from "next/navigation";
import { ArrowRightToLine, ArrowLeftToLine } from "lucide-react";
import { useEffect, useRef, useState } from "react";
import { Asset } from "@/src/interface/Asset";
import Loading from "@/src/components/loaders/loading";
import { fetcHome } from "@/src/libs/requests";

export default function HomeData() {
  const router = useRouter();

  const [assets, setAssets] = useState<Asset[]>([]);
  const [loading, setLoading] = useState(true);
  const [offset, setOffset] = useState(0);

  const offsetRef = useRef(0);

  useEffect(() => {
    offsetRef.current = offset;
  }, [offset]);

  async function fetchData() {
    // setIsLoading(true);
    try {
      const data = await fetcHome(offsetRef.current);
      setAssets(data);
      setLoading(false);
    } catch (error) {}
  }

  useEffect(() => {
    fetchData(); // Initial data fetch

    const intervalId = setInterval(() => {
      // Fetch data every 3 seconds
      fetchData();
    }, 5000);

    return () => {
      // Cleanup interval when component unmounts
      clearInterval(intervalId);
    };
  }, []); // Empty dependency array ensures useEffect runs only once on mount

  const handleNextPage = () => {
    setOffset(offset + 10);
    fetchData();
  };

  const handlePrevPage = () => {
    if (offset > 0) {
      setOffset(offset - 10);
      fetchData();
    }
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <>
      <div className="overflow-x-auto bg-base-200 w-full">
        <div className="max-w-[100vw]">
          <table className="table table-pin-rows">
            {/* head */}
            <thead>
              <tr className="font-bold">
                <th className="!static">
                  <label>rank</label>
                </th>
                <th className="!static">Currency</th>
                <th className="!static">Price</th>
                <th className="hidden md:table-cell">24h %</th>
                <th className="hidden lg:table-cell">Supply</th>
                <th className="hidden lg:table-cell">Max Supply</th>
                <th className="hidden md:table-cell">Market Cap</th>
                <th className="hidden md:table-cell">Volume(24h)</th>
              </tr>
            </thead>
            <tbody>
              {assets.map((asset) => (
                <tr
                  className="cursor-pointer hover:bg-[#2424243d] dark:hover:bg-[#70707044]"
                  key={asset.id}
                  onClick={() => router.push(`/currencies/${asset.id}`)}
                >
                  <th className="!static">
                    <label>{asset.rank}</label>
                  </th>
                  <td className="!static">
                    <div className="flex items-center space-x-3">
                      <div className="avatar">
                        <div
                          className={`${
                            parseFloat(asset.changePercent24Hr) > 0
                              ? "ring-success"
                              : "ring-error"
                          } rounded-full ring ring-offset-base-100 ring-offset-1 w-8 md:w-12 h-8 md:h-12`}
                        >
                          <img
                            src={`https://ik.imagekit.io/20xgx5cbk/crypto/${asset.symbol.toLowerCase()}@2x.png`}
                            alt={asset.symbol}
                            height={500}
                            width={500}
                            className="w-full h-full bg-zinc-300 rounded-lg dark:bg-zinc-600"
                          />
                        </div>
                      </div>
                      <div>
                        <div className="font-bold text-sm md:text-base">
                          {asset.name}
                        </div>
                        <div className="text-xs md:text-sm opacity-50">
                          {asset.symbol}
                        </div>
                      </div>
                    </div>
                  </td>
                  <td className="font-bold text-sm md:text-base !static">
                    $
                    {!isNaN(parseFloat(asset.priceUsd))
                      ? parseFloat(asset.priceUsd).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td
                    className={`hidden md:table-cell font-medium ${
                      parseFloat(asset.changePercent24Hr) > 0
                        ? "text-green-500"
                        : "text-red-600"
                    }`}
                  >
                    {!isNaN(parseFloat(asset.changePercent24Hr))
                      ? `${parseFloat(asset.changePercent24Hr).toFixed(2)}`
                      : "N/A"}{" "}
                    %
                  </td>
                  <td className="hidden lg:table-cell font-medium">
                    {!isNaN(parseFloat(asset.supply))
                      ? parseFloat(asset.supply).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td className="hidden lg:table-cell font-medium">
                    {!isNaN(parseFloat(asset.maxSupply))
                      ? parseFloat(asset.maxSupply).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-medium">
                    {!isNaN(parseFloat(asset.marketCapUsd))
                      ? parseFloat(asset.marketCapUsd).toLocaleString(
                          undefined,
                          {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          }
                        )
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-medium">
                    {!isNaN(parseFloat(asset.volumeUsd24Hr))
                      ? parseFloat(asset.volumeUsd24Hr).toLocaleString(
                          undefined,
                          {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          }
                        )
                      : "N/A"}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        <div className="join flex justify-center my-4 px-4">
          <div className="join grid grid-cols-2 w-full max-w-md">
            <button
              onClick={handlePrevPage}
              className="join-item btn btn-outline text-sm md:text-base"
            >
              <ArrowLeftToLine className="h-4 w-4 md:h-5 md:w-5" />
              <span className="hidden md:inline">Previous page</span>
              <span className="md:hidden">Prev</span>
            </button>
            <button
              onClick={handleNextPage}
              className="join-item btn btn-outline text-sm md:text-base"
            >
              <span className="hidden md:inline">Next page</span>
              <span className="md:hidden">Next</span>
              <ArrowRightToLine className="h-4 w-4 md:h-5 md:w-5" />
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
