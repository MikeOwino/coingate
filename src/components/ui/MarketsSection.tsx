"use client";

import { useCallback, useEffect, useState } from "react";
import { useParams } from "next/navigation";
import Loading from "@/src/components/loaders/loading";
import Link from "next/link";
import { Market } from "@/src/interface/Markets";
import { fetchMarket } from "@/src/libs/requests";

export default function MarketsSection() {
  const params = useParams();
  const mid = params.markets;

  const [markets, setMarkets] = useState<Market[]>([]); // Use an array for markets
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    try {
      const data = await fetchMarket(mid);
      setMarkets(data);
    } catch (error) {}

    setIsLoading(false);
  }, [mid]);

  useEffect(() => {
    fetchData(); // Call fetchData inside useEffect
  }, [fetchData]);

  if (markets.length === 0) {
    // Check if markets is an empty array
    return <Loading />;
  }
  return (
    <>
      <div className="min-h-screen bg-base-200">
        <div className="p-4 max-w-[100vw]">
          <table className="table table-pin-rows">
            {/* head */}
            <thead>
              <tr className="font-bold">
                <th className="!static">Exchange</th>
                <th className="!static">Price</th>
                <th className="hidden md:table-cell">Volume</th>
                <th className="hidden md:table-cell">Vol %</th>
              </tr>
            </thead>
            <tbody>
              {markets.map((market) => (
                <tr key={market.exchangeId}>
                  <td className="!static">
                    <div className="font-bold text-sm md:text-base">
                      {market.exchangeId}
                    </div>
                  </td>
                  <td className="!static">
                    <div className="font-bold text-sm md:text-base">
                      ${!isNaN(parseFloat(market.priceUsd))
                        ? parseFloat(market.priceUsd).toLocaleString(
                            undefined,
                            {
                              minimumFractionDigits: 2,
                              maximumFractionDigits: 2,
                            }
                          )
                        : "N/A"}
                    </div>
                  </td>
                  <td className="hidden md:table-cell font-bold text-sm md:text-base">
                    ${!isNaN(parseFloat(market.volumeUsd24Hr))
                      ? parseFloat(market.volumeUsd24Hr).toLocaleString(
                          undefined,
                          {
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                          }
                        )
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-medium text-sm md:text-base">
                    {!isNaN(parseFloat(market.volumePercent))
                      ? `${parseFloat(market.volumePercent).toFixed(2)}`
                      : "N/A"}{" "}
                    %
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
