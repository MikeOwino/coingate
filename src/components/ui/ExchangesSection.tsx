/* eslint-disable @next/next/no-img-element */
"use client";

import { useCallback, useEffect, useState } from "react";
import { useParams } from "next/navigation";
import Loading from "@/src/components/loaders/loading";
import Link from "next/link";
import { SquareArrowOutUpRight } from "lucide-react";
import { Hash } from "lucide-react";
import { Exchange } from "@/src/interface/Exchange";
import { fetchExchanges } from "@/src/libs/requests";

export default function ExchangeSection() {
  const params = useParams();
  const xid = params.exchanges;

  const [exchange, setExchange] = useState<Exchange | null>(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    try {
      const data = await fetchExchanges(xid);
      setExchange(data);
    } catch (error) {}

    setIsLoading(false);
  }, [xid]);

  useEffect(() => {
    fetchData(); // Call fetchData inside useEffect
  }, [fetchData]);

  if (!exchange) {
    return <Loading />;
  }
  return (
    <>
      <div className="min-h-screen bg-base-200 px-4">
        <div className="flex flex-col items-center py-8 md:py-16 gap-8">
          <div className="text-center">
            {exchange && (
              <>
                <h1 className="text-3xl md:text-5xl font-bold mb-4">{exchange.name}</h1>
                <p className="py-4">Exchange Information</p>
                <Link href="/exchanges">
                  <button className="btn btn-primary btn-sm md:btn-md">Exchanges</button>
                </Link>
              </>
            )}
          </div>

          <div className="stats stats-vertical md:stats-horizontal shadow w-full max-w-3xl">
            <div className="stat">
              <div className="stat-figure text-secondary">
                <div className="avatar">
                  <div
                    className={`${
                      parseFloat(exchange.percentTotalVolume) > 0
                        ? "ring-success"
                        : "ring-error"
                    } rounded-full ring ring-offset-base-100 ring-offset-1 w-8 md:w-12 h-8 md:h-12`}
                  >
                    <img
                      src="https://ik.imagekit.io/20xgx5cbk/crypto/btc@2x.png"
                      alt={exchange.exchangeId}
                      height={500}
                      width={500}
                      className="w-full h-full bg-zinc-300 rounded-lg dark:bg-zinc-600"
                    />
                  </div>
                </div>
              </div>
              <div className="stat-title text-sm md:text-base">Volume</div>
              <div className="stat-value text-base md:text-2xl">
                ${!isNaN(parseFloat(exchange.volumeUsd))
                  ? parseFloat(exchange.volumeUsd).toLocaleString(undefined, {
                      minimumFractionDigits: 0,
                      maximumFractionDigits: 0,
                    })
                  : "N/A"}
              </div>
            </div>

            <div className="stat">
              <div className="stat-figure text-primary">
                <Hash className="w-4 h-4 md:w-6 md:h-6" />
              </div>
              <div className="stat-title text-sm md:text-base">Rank</div>
              <div className="stat-value text-primary text-base md:text-2xl">
                {exchange.rank}
              </div>
            </div>

            <div className="stat">
              <div className="stat-title text-sm md:text-base">Explorer</div>
              <a href={exchange.exchangeUrl} target="_blank">
                <button className="btn btn-outline btn-accent btn-sm md:btn-md mt-2">
                  <SquareArrowOutUpRight className="w-4 h-4 md:w-5 md:h-5" />
                  <span className="hidden md:inline">Explorer</span>
                </button>
              </a>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
