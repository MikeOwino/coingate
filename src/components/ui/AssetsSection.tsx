/* eslint-disable @next/next/no-img-element */
"use client";

import { useCallback, useEffect, useState } from "react";
import { useParams } from "next/navigation";
import Loading from "@/src/components/loaders/loading";
import {
  Rocket,
  Globe,
  TrendingDown,
  TrendingUp,
  CircleDollarSign,
} from "lucide-react";
import { SquareArrowOutUpRight } from "lucide-react";
import TradingViewWidget from "@/src/components/ui/TradingViewWidget";
import { Metadata, ResolvingMetadata } from "next";
import { fetchAssets } from "@/src/libs/requests";
import { Asset } from "@/src/interface/Asset";

function formatLargeNumber(value: string): string {
  const num = parseFloat(value);
  if (isNaN(num)) return "N/A";

  if (num >= 1e12) return `$${(num / 1e12).toFixed(2)}T`;
  if (num >= 1e9) return `$${(num / 1e9).toFixed(2)}B`;
  if (num >= 1e6) return `$${(num / 1e6).toFixed(2)}M`;
  return `$${num.toFixed(2)}`;
}

export default function AssetComponent() {
  const params = useParams();
  const uid = params.asset;

  const [asset, setAsset] = useState<Asset | null>(null);
  const [isLoading, setIsLoading] = useState(true);

  const fetchData = useCallback(async () => {
    setIsLoading(true);
    try {
      const data = await fetchAssets(uid);
      setAsset(data);
    } catch (error) {}

    setIsLoading(false);
  }, [uid]);

  useEffect(() => {
    fetchData(); // Call fetchData inside useEffect
  }, [fetchData]);

  if (!asset) {
    return <Loading />;
  }
  return (
    <>
      <div className="grid grid-cols-1 gap-4">
        <div className="bg-base-200">
          <div className="hero-content text-center container mx-auto">
            <div className="max-w-md">
              {asset && (
                <>
                  <h1 className="text-5xl font-bold">{asset.name}</h1>
                  <p className="py-2 text-xl">Rank #{asset.rank}</p>
                </>
              )}
            </div>

            <div className="stats stats-vertical lg:stats-horizontal shadow">
              {/* Price Stats */}
              <div className="stat">
                <div className="stat-title">Current Price</div>
                <div className="stat-value text-primary">
                  $
                  {parseFloat(asset?.priceUsd || "0").toLocaleString(
                    undefined,
                    { minimumFractionDigits: 4, maximumFractionDigits: 4 }
                  )}
                </div>
                <div
                  className={`stat-desc ${
                    parseFloat(asset?.changePercent24Hr || "0") > 0
                      ? "text-success"
                      : "text-error"
                  }`}
                >
                  {parseFloat(asset?.changePercent24Hr || "0").toFixed(2)}%
                  (24h)
                </div>
              </div>

              {/* Market Cap */}
              <div className="stat">
                <div className="stat-title">Market Cap</div>
                <div className="stat-value text-secondary">
                  {formatLargeNumber(asset?.marketCapUsd || "0")}
                </div>
              </div>

              {/* Volume */}
              <div className="stat">
                <div className="stat-title">24h Volume</div>
                <div className="stat-value">
                  {formatLargeNumber(asset?.volumeUsd24Hr || "0")}
                </div>
              </div>
            </div>
          </div>

          {/* Additional Stats */}
          <div className="container mx-auto px-4 mb-8">
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
              {/* Supply Info */}
              <div className="card bg-base-100 shadow-xl">
                <div className="card-body">
                  <h2 className="card-title">
                    <CircleDollarSign /> Supply Information
                  </h2>
                  <p>Supply: {formatLargeNumber(asset?.supply || "0")}</p>
                  <p>
                    Max Supply: {formatLargeNumber(asset?.maxSupply || "0")}
                  </p>
                </div>
              </div>

              {/* Links */}
              <div className="card bg-base-100 shadow-xl">
                <div className="card-body">
                  <h2 className="card-title">
                    <Globe /> Links
                  </h2>
                  <div className="flex gap-2">
                    <a
                      href={asset?.explorer}
                      target="_blank"
                      className="btn btn-outline btn-sm"
                    >
                      <SquareArrowOutUpRight />
                      Explorer
                    </a>
                  </div>
                </div>
              </div>

              {/* Price Stats */}
              <div className="card bg-base-100 shadow-xl">
                <div className="card-body">
                  <h2 className="card-title">Price Statistics</h2>
                  <div className="flex items-center gap-2">
                    <TrendingUp className="text-success" />
                    <p>
                      VWAP (24h): $
                      {parseFloat(asset?.vwap24Hr || "0").toFixed(4)}
                    </p>
                  </div>
                  <div className="flex items-center gap-2">
                    <CircleDollarSign />
                    <p>
                      Current: ${parseFloat(asset?.priceUsd || "0").toFixed(4)}
                    </p>
                  </div>
                  <div className="text-xs mt-2 text-base-content/70">
                    VWAP: Volume Weighted Average Price
                  </div>
                </div>
              </div>
            </div>
          </div>

          {/* Chart */}
          <div className="hidden bg-cover lg:block w-full h-[70vh] mt-2 container mx-auto">
            <TradingViewWidget symbol={`KUCOIN:${asset?.symbol}USDT`} />
          </div>
        </div>
      </div>
    </>
  );
}
