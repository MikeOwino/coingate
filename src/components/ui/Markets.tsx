/* eslint-disable @next/next/no-img-element */
"use client";

import { useEffect, useState } from "react";
import Loading from "@/src/components/loaders/loading";
import Link from "next/link";
import { Asset } from "@/src/interface/Asset";
import { fetchMarketSect } from "@/src/libs/requests";

export default function MarketSect() {
  const [assets, setAssets] = useState<Asset[]>([]);

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    // setIsLoading(true);
    try {
      const data = await fetchMarketSect();
      setAssets(data);
    } catch (error) {}
  }

  if (!assets.length) {
    return <Loading />;
  }

  return (
    <>
      <div className="overflow-x-auto bg-base-200 w-full">
        <div className="max-w-[100vw]">
          <table className="table table-pin-rows">
            {/* head */}
            <thead>
              <tr className="font-bold">
                <th className="!static">
                  <label>rank</label>
                </th>
                <th className="!static">Currency</th>
                <th className="!static">Price</th>
                <th className="hidden md:table-cell">24h %</th>
                <th className="hidden lg:table-cell">Supply</th>
                <th className="hidden md:table-cell">Market Cap</th>
                <th className="hidden md:table-cell">Volume(24h)</th>
              </tr>
            </thead>
            <tbody>
              {assets.map((asset) => (
                <tr key={asset.id}>
                  <th className="!static">
                    <label>{asset.rank}</label>
                  </th>
                  <td className="!static">
                    <Link href={`/markets/${asset.id}`}>
                      <div className="flex items-center space-x-3">
                        <div className="avatar">
                          <div
                            className={`${
                              parseFloat(asset.changePercent24Hr) > 0
                                ? "ring-success"
                                : "ring-error"
                            } rounded-full ring ring-offset-base-100 ring-offset-1 w-8 md:w-12 h-8 md:h-12`}
                          >
                            <img
                              src={`https://ik.imagekit.io/20xgx5cbk/crypto/${asset.symbol.toLowerCase()}@2x.png`}
                              alt={asset.symbol}
                              height={500}
                              width={500}
                              className="w-full h-full bg-zinc-300 rounded-lg dark:bg-zinc-600"
                            />
                          </div>
                        </div>
                        <div>
                          <div className="font-bold text-sm md:text-base">{asset.name}</div>
                          <div className="text-xs md:text-sm opacity-50">{asset.symbol}</div>
                        </div>
                      </div>
                    </Link>
                  </td>
                  <td className="font-bold text-sm md:text-base !static">
                    ${!isNaN(parseFloat(asset.priceUsd))
                      ? parseFloat(asset.priceUsd).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td
                    className={`hidden md:table-cell font-medium ${
                      parseFloat(asset.changePercent24Hr) > 0
                        ? "text-green-500"
                        : "text-red-600"
                    }`}
                  >
                    {!isNaN(parseFloat(asset.changePercent24Hr))
                      ? `${parseFloat(asset.changePercent24Hr).toFixed(2)}`
                      : "N/A"}{" "}
                    %
                  </td>
                  <td className="hidden lg:table-cell font-medium">
                    {!isNaN(parseFloat(asset.supply))
                      ? parseFloat(asset.supply).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-medium">
                    {!isNaN(parseFloat(asset.marketCapUsd))
                      ? parseFloat(asset.marketCapUsd).toLocaleString(undefined, {
                          minimumFractionDigits: 2,
                          maximumFractionDigits: 2,
                        })
                      : "N/A"}
                  </td>
                  <td className="hidden md:table-cell font-medium">
                    {!isNaN(parseFloat(asset.volumeUsd24Hr))
                      ? parseFloat(asset.volumeUsd24Hr).toLocaleString(
                          undefined,
                          {
                            minimumFractionDigits: 2,
                            maximumFractionDigits: 2,
                          }
                        )
                      : "N/A"}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
}
