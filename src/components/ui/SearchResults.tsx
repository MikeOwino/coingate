/* eslint-disable @next/next/no-img-element */
import Link from "next/link";
import { Asset } from "@/src/interface/Asset";

interface SearchResultsProps {
  results: Asset[];
  isVisible: boolean;
  onResultClick: () => void;
}

export default function SearchResults({
  results,
  isVisible,
  onResultClick,
}: SearchResultsProps) {
  if (!isVisible || results.length === 0) return null;

  return (
    <div className="absolute top-full left-0 mt-1 w-64 max-h-96 overflow-y-auto bg-base-100 rounded-lg shadow-lg border border-base-300 z-30">
      {results.map((asset) => (
        <Link
          key={asset.id}
          href={`/currencies/${asset.id}`}
          onClick={onResultClick}
          className="flex items-center gap-2 p-2 hover:bg-base-200 transition-colors"
        >
          <div className="avatar">
            <div className="w-8 h-8 rounded-full">
              <img
                src={`https://ik.imagekit.io/20xgx5cbk/crypto/${asset.symbol.toLowerCase()}@2x.png`}
                alt={asset.symbol}
                className="bg-zinc-300 dark:bg-zinc-600"
              />
            </div>
          </div>
          <div className="flex flex-col">
            <span className="font-medium">{asset.name}</span>
            <span className="text-sm opacity-70">{asset.symbol}</span>
          </div>
        </Link>
      ))}
    </div>
  );
}
