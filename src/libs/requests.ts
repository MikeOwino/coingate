"use server";

const commonOptions = {
    method: "GET",
    headers: {
        accept: "application/json",
        Authorization: `Bearer ${process.env.COINCAP_API_KEY}`,
    },
};

export async function fetchAssets(uid: any | null) {
    const options = {
        url: `${process.env.API_URL}/assets/${uid}`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetchExchanges(xid: any | null) {
    const options = {
        url: `${process.env.API_URL}/exchanges/${xid}`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetchMarket(mid: any | null) {
    const options = {
        url: `${process.env.API_URL}/assets/${mid}/markets`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetchTrending() {
    const options = {
        url: `${process.env.API_URL}/assets?limit=200`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetchExchange() {
    const options = {
        url: `${process.env.API_URL}/exchanges`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetchMarketSect() {
    const options = {
        url: `${process.env.API_URL}/assets?limit=100`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetcNavData() {
    const options = {
        url: `${process.env.API_URL}/assets?limit=1000`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}

export async function fetcHome(offsetRef: any | null) {
    const options = {
        ...commonOptions,
        url: `${process.env.API_URL}/assets?limit=10&offset=${offsetRef}`,
    };
    const response = await fetch(options.url);
    const results = await response.json();
    return results.data;
}