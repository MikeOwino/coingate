/* eslint-disable @next/next/no-img-element */
"use client";

import { useEffect, useState } from "react";
// import Link from "next/link";

import Loading from "@/src/components/loaders/loading";

export default function HotList() {
  const [countdown, setCountdown] = useState<string | null>(null);
  useEffect(() => {
    // Set the target date and time for the countdown
    const targetDate = new Date("April 19, 2024 09:46:53 UTC").getTime();

    // Update the countdown every second
    const interval = setInterval(() => {
      const now = new Date().getTime();
      const distance = targetDate - now;

      if (distance < 0) {
        // Countdown has ended
        clearInterval(interval);
        setCountdown("PUMP IT!");
      } else {
        // Calculate remaining time
        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Update the countdown state
        setCountdown(
          `${days} days, ${hours} hours, ${minutes} minutes, ${seconds} seconds`
        );
      }
    }, 1000);

    // Clean up the interval on component unmount
    return () => clearInterval(interval);
  }, []);

  return (
    <>
      <div className="min-h-screen h-max">
        <h1 className="text-2xl font-bold text-center">
          On April 2024 the block reward will be reduced from 6.25 BTC per block
          to 3.125 BTC per block
        </h1>
        <section className="mt-12 hero">
          <div className="card w-2/5 bg-base-100 shadow-xl image-full">
            <figure>
              <img width={1200} height={750} src="/btc.png" alt="car!" />
            </figure>
            <div className="card-body">
              <p className="card-title text-3xl font-semibold">
                BITCOIN HALVING 2024
              </p>
              <p className="text-3xl font-extrabold text-green-500">
                {" "}
                {countdown && <h1 className="countdown ">{countdown}</h1>}
              </p>
              <div className="card-actions justify-end">
                <button
                  className="btn btn-primary"
                  onClick={() =>
                    window.open(
                      "https://www.binance.com/en/trade/BTC_USDT",
                      "_blank"
                    )
                  }
                >
                  Buy Now
                </button>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}
