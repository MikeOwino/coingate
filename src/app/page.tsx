import HomeData from "../components/ui/Home";

export default function Home() {
  return (
    <>
      <HomeData />
    </>
  );
}
