import AssetComponent from "@/src/components/ui/AssetsSection";
import { Metadata, ResolvingMetadata } from "next";
import { fetchAssets } from "@/src/libs/requests";

type Props = {
  params: { asset: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const uid = params.asset;

  // fetch data
  const pageName = await fetchAssets(uid);
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: `${pageName?.name} (${pageName?.symbol}) | CoinZinger`,
    description: pageName?.explorer,
    openGraph: {
      title: pageName?.name,
      description: pageName?.explorer,
    },
  };
}

export default function AssetPage() {
  return (
    <>
      <AssetComponent />
    </>
  );
}
