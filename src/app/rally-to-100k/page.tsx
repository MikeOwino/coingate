/* eslint-disable @next/next/no-img-element */
"use client";
import React, { useEffect, useState, useCallback } from "react";
import { Rocket, SquareArrowOutUpRight } from "lucide-react";
import Confetti from "react-confetti";

interface BitcoinData {
  name: string;
  changePercent24Hr: string;
  priceUsd: string;
  marketCapUsd: string;
  supply: string;
  symbol: string;
  rank: number;
  volumeUsd24Hr: string;
  website: string;
  explorer: string;
}

const BitcoinPriceUpdate = () => {
  const [bitcoinData, setBitcoinData] = useState<BitcoinData | null>(null);
  const [loading, setLoading] = useState(true);
  const [showConfetti, setShowConfetti] = useState(false);
  const [windowDimensions, setWindowDimensions] = useState({
    width: 0,
    height: 0,
  });

  const fetchBitcoinData = useCallback(async () => {
    setLoading(true);
    try {
      const response = await fetch("https://graphql.coincap.io/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          variables: { id: "bitcoin" },
          query: `
            query ($id: ID!) {
              asset(id: $id) {
                name
                changePercent24Hr
                priceUsd
                marketCapUsd
                supply
                symbol
                rank
                volumeUsd24Hr
                website
                explorer
                __typename
              }
            }
          `,
        }),
      });

      const result = await response.json();
      setBitcoinData(result.data.asset);
    } catch (error) {
      console.error("Error fetching Bitcoin data:", error);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchBitcoinData();
    const intervalId = setInterval(fetchBitcoinData, 1000); // Fetch every 10 seconds
    return () => clearInterval(intervalId);
  }, [fetchBitcoinData]);

  // Ensure hooks are called unconditionally
  const currentPrice = bitcoinData ? parseFloat(bitcoinData.priceUsd) : 0;
  const targetPrice = 100000;
  const difference = targetPrice - currentPrice;

  useEffect(() => {
    if (bitcoinData && currentPrice >= targetPrice) {
      setShowConfetti(true);
    } else {
      setShowConfetti(false);
    }
  }, [bitcoinData, currentPrice, targetPrice]);

  useEffect(() => {
    const updateWindowDimensions = () => {
      setWindowDimensions({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };

    // Set initial dimensions
    updateWindowDimensions();

    // Add event listener
    window.addEventListener("resize", updateWindowDimensions);

    // Cleanup
    return () => window.removeEventListener("resize", updateWindowDimensions);
  }, []);

  if (!bitcoinData) {
    return <div>Loading...</div>;
  }

  return (
    <div className="container mx-auto px-2 py-4 md:p-4 min-h-screen flex flex-col items-center relative">
      {showConfetti && (
        <div className="fixed inset-0 pointer-events-none z-50">
          <Confetti
            width={windowDimensions.width}
            height={windowDimensions.height}
            recycle={true}
            numberOfPieces={200}
          />
        </div>
      )}
      <div className="grid grid-cols-1 gap-4 w-full max-w-4xl">
        <div className="bg-base-200 p-4 md:p-8 rounded-lg shadow-md">
          <div className="text-center mb-4 md:mb-8">
            <h1 className="text-2xl md:text-5xl font-bold flex flex-wrap items-center justify-center gap-2">
              <span className="inline-flex items-center">
                {bitcoinData.name} ({bitcoinData.symbol})
              </span>
              <span className="inline-flex items-center">
                Rally to $100k
                <Rocket
                  size={24}
                  className="text-green-500 ml-2 animate-bounce md:w-10 md:h-10"
                />
              </span>
            </h1>
          </div>
        </div>
        <div className="stats shadow w-full flex-wrap justify-center">
          <div className="stat">
            <div className="stat-figure text-secondary">
              <div className="avatar">
                <div
                  className={`${
                    parseFloat(bitcoinData.changePercent24Hr) > 0
                      ? "ring-success"
                      : "ring-error"
                  } rounded-full ring ring-offset-base-100 ring-offset-2 w-16 md:w-20 h-16 md:h-20`}
                >
                  <img
                    src={`https://ik.imagekit.io/20xgx5cbk/crypto/${bitcoinData.symbol.toLowerCase()}@2x.png`}
                    alt={bitcoinData.symbol}
                    className="w-full h-full bg-zinc-300 rounded-lg dark:bg-zinc-600"
                  />
                </div>
              </div>
            </div>
            <div className="stat-title">24Hr Change</div>
            <div
              className={`stat-value text-primary ${
                parseFloat(bitcoinData.changePercent24Hr) > 0
                  ? "text-green-500"
                  : "text-red-600"
              }`}
            >
              {parseFloat(bitcoinData.changePercent24Hr).toFixed(2)}%
            </div>
            <div className="stat-desc">
              {parseFloat(bitcoinData.changePercent24Hr) > 0
                ? "Positive change"
                : "Negative change"}
            </div>
          </div>

          <div className="stat">
            <div className="stat-title">Current Price</div>
            <div className="stat-value text-secondary">
              $
              {currentPrice.toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            </div>
            <div className="stat-desc">Current market price</div>
          </div>

          <div className="stat">
            <div className="stat-title">Difference to $100k</div>
            <div className="stat-value">
              $
              {difference.toLocaleString(undefined, {
                minimumFractionDigits: 2,
                maximumFractionDigits: 2,
              })}
            </div>
            <div className="stat-desc text-secondary">
              Amount needed to reach $100k
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BitcoinPriceUpdate;
