import type { Metadata } from "next";
import LossList from "@/src/components/ui/Losers";

export const metadata: Metadata = {
  title: "Losers | CoinZinger",
  description: "CoinZinger",
};

export default function Losers() {
  return (
    <>
      <LossList />
    </>
  );
}
