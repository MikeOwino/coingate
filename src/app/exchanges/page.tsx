import ExchangeData from "@/src/components/ui/Exchanges";
import type { Metadata } from "next";

export const metadata: Metadata = {
  title: "Exchanges | CoinZinger",
  description: "CoinZinger",
};

export default function Exchange() {
  return (
    <>
      <ExchangeData />
    </>
  );
}
