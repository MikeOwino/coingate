import ExchangeSection from "@/src/components/ui/ExchangesSection";
import { Metadata, ResolvingMetadata } from "next";
import { fetchExchanges } from "@/src/libs/requests";

type Props = {
  params: { exchanges: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const xid = params.exchanges;

  // fetch data
  const pageName = await fetchExchanges(xid);
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: `${pageName?.name} | CoinZinger`,
    description: pageName?.exchangeUrl,
    openGraph: {
      title: pageName?.name,
      description: pageName?.exchangeUrl,
    },
  };
}

export default function ExchangePage() {
  return (
    <>
      <ExchangeSection />
    </>
  );
}
