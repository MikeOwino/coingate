import type { Metadata } from "next";
import MarketSect from "@/src/components/ui/Markets";

export const metadata: Metadata = {
  title: "Markets | CoinZinger",
  description: "CoinZinger",
};

export default function Markets() {
  return (
    <>
      <MarketSect />
    </>
  );
}
