import MarketsSection from "@/src/components/ui/MarketsSection";
import { Metadata, ResolvingMetadata } from "next";
import { fetchMarket } from "@/src/libs/requests";

type Props = {
  params: { markets: string };
  searchParams: { [key: string]: string | string[] | undefined };
};

export async function generateMetadata(
  { params }: Props,
  parent: ResolvingMetadata
): Promise<Metadata> {
  // read route params
  const mid = params.markets;

  // fetch data
  const pageName = await fetchMarket(mid);
  const previousImages = (await parent).openGraph?.images || [];

  return {
    title: `${mid} Markets | CoinZinger`,
    description: pageName?.baseId,
    openGraph: {
      title: pageName?.baseId,
      description: pageName?.baseId,
    },
  };
}

export default function MarketPage() {
  return (
    <>
      <MarketsSection />
    </>
  );
}
