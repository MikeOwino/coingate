import "./globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Header from "@/src/components/layout/header";
import Nav from "@/src/components/layout/nav";
import Footer from "@/src/components/layout/footer";
import Script from "next/script";

const inter = Inter({ subsets: ["latin"] });

// cloudflare
// export const runtime = "edge";

export const metadata: Metadata = {
  title: "CoinZinger | Top Cryptocurrency Prices and Charts",
  description: "Top Cryptocurrency Prices and Charts ",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>
        {/* <ThemeProvider attribute="class" defaultTheme="system" enableSystem> */}
        <Header />
        <Nav />
        <main>{children}</main>
        <Footer />
        {/* </ThemeProvider> */}
      </body>
      {/* <Script
        defer
        src="https://stats.hackhub.buzz/script.js"
        data-website-id="c0deef65-f45d-4450-9d68-71e576a0ca8a"
      /> */}
    </html>
  );
}
