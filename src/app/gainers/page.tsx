import type { Metadata } from "next";
import HotList from "@/src/components/ui/Gainers";

export const metadata: Metadata = {
  title: "Gainers | CoinZinger",
  description: "CoinZinger",
};

export default function Gainers() {
  return (
    <>
      <HotList />
    </>
  );
}
